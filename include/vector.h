/* Definicion del tipo de datos vector3D */
typedef enum { false, true } bool;
typedef struct
{
	float x;
	float y;
	float z;
} vector3D;

float dotproduct(vector3D v1, vector3D v2);
vector3D crossproduct(vector3D v1, vector3D v2);
float magnitud(vector3D v1);
int esOrtogonal(vector3D v1, vector3D v2);

