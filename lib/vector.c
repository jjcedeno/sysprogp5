/* implementar aquí las funciones requeridas */
#include <math.h>
#include "../include/vector.h"

float dotproduct(vector3D v1, vector3D v2){
    float producto = 0.0;
    producto = v1.x*v2.x + v1.y*v2.y + v1.z+v2.z; 
    return producto;
}

vector3D crossproduct(vector3D v1, vector3D v2)
{
    vector3D vfinal;
    vfinal.x = v1.y * v2.z - v1.z * v2.z;
    vfinal.y = v1.z * v2.x - v1.x * v2.z;
    vfinal.z = v1.x * v2.y - v1.y * v2.x;
    return vfinal;
}

float magnitud(vector3D v1){
    float magnitud=0.0f;
    magnitud= sqrt(pow(v1.x,2)+pow(v1.y,2)+pow(v1.z,2));
    return magnitud;
}

int esOrtogonal(vector3D v1, vector3D v2){
    	int ortogonal=0;
	ortogonal = v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
	return ortogonal==0? 1:0;
}
