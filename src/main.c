#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "vector.h"
int main(){
    	char p1[25],p2[25];
	char *token;
	char *x,*y,*z,*x2,*y2,*z2;
	
	printf("Ingrese vector 1 x,y,z ejemplo 1,2,3: ");
	fgets(p1,sizeof p1 ,stdin);
	printf("Ingrese vector 2 x,y,z ejemplo 3,4,5: ");
	fgets(p2,sizeof p2,stdin);
	
	vector3D v1;
	vector3D v2;
	v1.x= atof(strtok(p1, ","));
	v1.y= atof(strtok(NULL, ","));
	v1.z= atof(strtok(NULL,","));
	v2.x= atof(strtok(p2, ","));
	v2.y= atof(strtok(NULL, ","));
	v2.z= atof(strtok(NULL,","));
	    
	printf("Producto escalar =  %.2f \n", dotproduct(v1,v2));
	vector3D vtemp=crossproduct(v1,v2);
	printf("Producto vectorial =  %.2f %.2f %.2f \n", vtemp.x,vtemp.y,vtemp.z);
	printf("Magnitud del vector =  %.2f \n", magnitud(v1));
	printf("Es ortogonal v1,v2  =  %s \n",	 esOrtogonal(v1,v2)==1?"si":"no");

}
