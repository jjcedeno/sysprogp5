# Este es el archivo Makefile de la práctica, editarlo como sea neceario
IDIR =include
CC=gcc
CFLAGS=-I$(IDIR)
ODIR=obj
LDIR = lib/vector.c
SDIR = src/main.c
LIBS=-lm

# Target que se ejecutará por defecto con el comando make
all: title dynamic clean

title:
	$(info *******CRISTIAN PENA*********)
	$(info *******JOHN CEDENO  *********)
# Target que compilará el proyecto usando librerías estáticas
static: 
	$(info **********STATIC*************)
	@make title	
	@$(CC) -c $(LDIR) $(CFLAGS) 
	@ar rcs libvector.a vector.o
	@$(CC) -c $(SDIR) $(CFLAGS) 
	@$(CC) -static -o vector main.o ./libvector.a $(CFLAGS) $(LIBS)
	@make clean -s
# Target que compilará el proyecto usando librerías dinámicas
dynamic:
	$(info **********DYNAMIC************)
	@$(CC) -shared -fPIC -o libvector.so $(LDIR) 
	@$(CC) -o vector $(SDIR) ./libvector.so -I$(INCLUDES)vector.h $(CFLAGS) $(LIBS)

# Limpiar archivos temporales
.PHONY: clean
clean:
	@rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ *.o 
